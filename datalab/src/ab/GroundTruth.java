package ab;

import ab.vision.ABObject;
import ab.vision.ABType;
import ab.demo.ObjectClassifierModel;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.awt.*;
import java.awt.geom.Area;
import java.util.*;
import java.util.List;

public class GroundTruth {

    //private JSONArray gt;
    private LinkedList _birds = new LinkedList<ABObject>();
    private LinkedList _pigs = new LinkedList<ABObject>();
    private LinkedList _hills = new LinkedList<ABObject>();
    private LinkedList _allBlocks = new LinkedList<ABObject>();
    private LinkedList _tnts = new LinkedList<ABObject>();
    private Rectangle _sling = new Rectangle();
    private int _ground = 0;

    private double[][] objectColorMatrix;
    private ABType[] objectType;

    public static int indexOfSmallest(double[] array){

        // add this
        if (array.length == 0)
            return -1;

        int index = 0;
        double min = array[index];

        for (int i = 1; i < array.length; i++){
            if (array[i] <= min){
                min = array[i];
                index = i;
            }
        }
        return index;
    }

    public int indexOfLargest( double[] array )
    {
        if ( array == null || array.length == 0 ) return -1; // null or empty

        int largest = 0;
        for ( int i = 1; i < array.length; i++ )
        {
            if ( array[i] > array[largest] ) largest = i;
        }
        return largest; // position of the first largest found
    }

    private void findObjectType(JSONArray gt) {


        HashMap<String,ABType> tempDict = new HashMap<String,ABType>();

        tempDict.put("Ground",ABType.Ground);
        tempDict.put("Slingshot",ABType.Sling);


        JSONObject objects = (JSONObject) gt.get(0);
        JSONArray objectsFeatures = (JSONArray)objects.get("features");
        objectType = new ABType[objectsFeatures.size()];
        objectColorMatrix = new double[objectsFeatures.size()][256];
        //create object color matrix
        for (int i = 0; i < objectsFeatures.size(); i++)
        {
            JSONObject obj = (JSONObject) objectsFeatures.get(i);

            JSONObject objProperties = (JSONObject) obj.get("properties");
            JSONObject objGeometry = (JSONObject) obj.get("geometry");
            String typeName = (String) objProperties.get("label");
            if (typeName.equals("Ground") || typeName.equals("Slingshot"))
            {
                objectType[i] = tempDict.get(typeName);
                continue;
            }

            else if (typeName.equals("Trajectory"))
            {
                continue;
            }
            else
            {
                JSONArray colormapArray = (JSONArray)objProperties.get("colormap");
                for (int j = 0 ; j < colormapArray.size() ; j++)
                {
                    JSONObject cl = (JSONObject) colormapArray.get(j);
                    int color = Math.toIntExact((long)cl.get("color"));
                    double percent = (double) cl.get("percent");
                    objectColorMatrix[i][color] = percent;
                }


                //calculate the theta distance against all of the template vectors
                double[] interResult = new double[ObjectClassifierModel.target_class.length];

                for (int templateObj = 0 ; templateObj < ObjectClassifierModel.model_coef.length ; templateObj ++)
                {
                    for (int templateColor = 0 ; templateColor < ObjectClassifierModel.model_coef[templateObj].length; templateColor++)
                    {
                        //System.out.println("templatedObj length" + ObjectClassifierModel.model_coef[templateObj].length);
                        if (templateColor == 0) {
                            //bias term
                            interResult[templateObj] = ObjectClassifierModel.model_coef[templateObj][templateColor];
                        }
                        else{
                            interResult[templateObj] += objectColorMatrix[i][templateColor-1] * ObjectClassifierModel.model_coef[templateObj][templateColor];
                        }

                    }

                }

                // find the minimum index and use the corrsponding type
                objectType[i] = ObjectClassifierModel.target_class[indexOfLargest(interResult)];

            }

        }

    }

    public GroundTruth(JSONArray gt){

        findObjectType(gt);
        JSONObject objects = (JSONObject) gt.get(0);
        JSONArray objectsFeatures = (JSONArray)objects.get("features");

        for (int i = 0 ; i < objectsFeatures.size(); i++) {
            JSONObject obj = (JSONObject)objectsFeatures.get(i);
            JSONObject objProperties = (JSONObject) obj.get("properties");
            JSONObject objGeometry = (JSONObject) obj.get("geometry");
            String typeName = (String)objProperties.get("label");

            if (typeName.equals("Ground")){
                _ground = (int)  Math.round((long)objProperties.get("yindex"));
            }

            else if (typeName.equals("Slingshot")) {
                JSONArray ver = (JSONArray)objGeometry.get("coordinates");
                JSONArray verList = (JSONArray)ver.get(0);
                for (int j = 0 ; j < verList.size() ; j++){

                    JSONArray point = (JSONArray) verList.get(j);
                    long x = (long) point.get(0);
                    long y = (long) point.get(1);
                    Point p = new Point((int)x,(int)(y));
                    if (_sling.x==0 && _sling.y==0){
                        _sling = new Rectangle((int)x,(int)y,0,0);
                    }
                    else{
                        _sling.add(p);

                    }
                }
            }

            else if (typeName.equals("Trajectory")) {
                continue;
            }

            else
                {
                // find the type of object by using color distribution
                ABType identifiedType = ABType.Unknown;
                switch (objectType[i]){
                    case RedBird:
                    case BlueBird:
                    case BlackBird:
                    case WhiteBird:
                    case YellowBird:
                        ABObject bird = new ABObject();
                        JSONArray birdver = (JSONArray)objGeometry.get("coordinates");
                        JSONArray birdVerList = (JSONArray)birdver.get(0);

                        //System.out.println(ver);
                        for (int j = 0 ; j < birdVerList.size() ; j++) {
                            JSONArray point = (JSONArray) birdVerList.get(j);
                            long x = (long) point.get(0);
                            long y = (long) point.get(1);
                            Point p = new Point((int)x,(int)(y));

                            if (bird.x == 0 && bird.y == 0) {
                                bird.x = (int) x;
                                bird.y = (int) y; // as the ground truth start from the bottom left rather than top left
                                //System.out.println(bird);
                            } else {
                                bird.add(p);
                                //System.out.println(bird);
                            }
                        }

                        bird.type = objectType[i];

                        //System.out.println(bird.type);

                        _birds.add(bird);

                        break;

                    case Hill:
                        ABObject platform = new ABObject();
                        JSONArray platformver = (JSONArray)objGeometry.get("coordinates");
                        JSONArray platformVerList = (JSONArray)platformver.get(0);

                        //System.out.println(ver);
                        for (int j = 0 ; j < platformVerList.size() ; j++){
                            JSONArray point = (JSONArray) platformVerList.get(j);
                            long x = (long) point.get(0);
                            long y = (long) point.get(1);
                            Point p = new Point((int)x,(int)y);
                            if (platform.x == 0 && platform.y == 0) {
                                platform.x = (int) x;
                                platform.y = (int) y;
                                //System.out.println(bird);
                            } else {
                                platform.add(p); // as the ground truth start from the bottom left rather than top left
                                //System.out.println(bird);
                            }
                        }
                        platform.type = objectType[i];
                        _hills.add(platform);

                        break;

                    case Wood:
                    case Ice:
                    case Stone:
                        ABObject block = new ABObject();
                        JSONArray blockver = (JSONArray)objGeometry.get("coordinates");
                        JSONArray blockVerList = (JSONArray)blockver.get(0);
                        //System.out.println(ver);
                        for (int j = 0 ; j < blockVerList.size() ; j++){
                            JSONArray point = (JSONArray) blockVerList.get(j);
                            long x = (long) point.get(0);
                            long y = (long) point.get(1);
                            Point p = new Point((int)x,(int)(y));
                            if (block.x == 0 && block.y == 0) {
                                block.x = (int) x;
                                block.y = (int) y;
                                //System.out.println(bird);
                            } else {
                                block.add(p);
                                //System.out.println(bird);
                            }
                        }
                        Rectangle blockmbr = new Rectangle(block);
                        block._ar = new Area(blockmbr);
                        blockmbr.grow(block.shift,block.shift);
                        block._shiftar = new Area(blockmbr);
                        block.type = objectType[i];
                        _allBlocks.add(block);
                        break;

                    case TNT:
                        ABObject tnt = new ABObject();
                        JSONArray tntver = (JSONArray)objGeometry.get("coordinates");
                        JSONArray tntVerList = (JSONArray)tntver.get(0);

                        //System.out.println(ver);
                        for (int j = 0 ; j < tntVerList.size() ; j++){
                            JSONArray point = (JSONArray) tntVerList.get(j);
                            long x = (long) point.get(0);
                            long y = (long) point.get(1);
                            Point p = new Point((int)x,(int)(y));
                            if (tnt.x == 0 && tnt.y == 0) {
                                tnt.x = (int) x;
                                tnt.y = (int) y;
                                //System.out.println(bird);
                            } else {
                                tnt.add(p); // as the ground truth start from the bottom left rather than top left
                                //System.out.println(bird);
                            }
                        }

                        Rectangle tntmbr = new Rectangle(tnt);
                        tnt._ar = new Area(tntmbr);
                        tntmbr.grow(tnt.shift,tnt.shift);
                        tnt._shiftar = new Area(tntmbr);
                        tnt.type = objectType[i];
                        _tnts.add(tnt);
                        break;

                    case Pig:
                        ABObject pig = new ABObject();
                        JSONArray pigver = (JSONArray)objGeometry.get("coordinates");
                        
                        JSONArray pigVerList = (JSONArray)pigver.get(0);

                        //System.out.println(ver);
                        for (int j = 0 ; j < pigVerList.size() ; j++){
                            JSONArray point = (JSONArray) pigVerList.get(j);
                            long x = (long) point.get(0);
                            long y = (long) point.get(1);
                            Point p = new Point((int)x,(int)(y));

                            if (pig.x == 0 && pig.y == 0) {
                                pig.x = (int) x;
                                pig.y = (int) y;
                                //System.out.println(bird);
                            } else {
                                pig.add(p); // as the ground truth start from the bottom left rather than top left
                                //System.out.println(bird);
                            }
                        }

                        Rectangle pigmbr = new Rectangle(pig);
                        pig._ar = new Area(pigmbr);
                        pigmbr.grow(pig.shift,pig.shift);
                        pig._shiftar = new Area(pigmbr);
                        pig.type = objectType[i];
                        _pigs.add(pig);
                        break;

                    case Unknown:
                        System.out.println("unknown object");
                        break;
                    }
                }
        }
    }

    public ABType getBirdTypeOnSling() {

        if (this._birds.isEmpty()) {
            return ABType.Unknown;
        }

        Collections.sort(this._birds, new Comparator<Rectangle>() {
            @Override
            public int compare(Rectangle o1, Rectangle o2) {
                return ((Integer) (o1.y)).compareTo((o2.y));
            }
        });

        Rectangle sling = _sling;

        ABObject possibleBirdOnSling = (ABObject)this._birds.get(0);



//        if (sling != null && sling.x < possibleBirdOnSling.x
//                && sling.x + sling.width > (int) possibleBirdOnSling.getCenterX()) {
//            return possibleBirdOnSling.type;
//        }

        return possibleBirdOnSling.type;
    }

    public int getGroundLevel() {

        return _ground;
    }


    public List<ABObject> findTNTs() {
        return _tnts;
    }


    public List<ABObject> findPigsRealShape() {
        return _pigs;
    }

    public List<ABObject> findBirdsRealShape() {
        return _birds;
    }

    public List<ABObject> findHills() {
        return _hills;
    }


    public Rectangle findSlingshotRealShape() {
        return _sling;
    }


    public List<ABObject> findBlocksRealShape() {
        return _allBlocks;
    }


}
