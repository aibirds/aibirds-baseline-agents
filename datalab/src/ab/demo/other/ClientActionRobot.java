/*****************************************************************************
 ** ANGRYBIRDS AI AGENT FRAMEWORK
 ** Copyright (c) 2015,  XiaoYu (Gary) Ge, Stephen Gould,Jochen Renz
 ** Sahan Abeyasinghe, Jim Keys,   Andrew Wang, Peng Zhang
 ** Team DataLab Birds: Karel Rymes, Radim Spetlik, Tomas Borovicka
 ** Team HeartyTian: Tian Jian Wang
 ** All rights reserved.
 **This work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License.
 **To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-sa/3.0/
 *or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.
 *****************************************************************************/

package ab.demo.other;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import ab.demo.ClientNaiveAgent;
import ab.planner.TrajectoryPlanner;
import ab.vision.ABObject;
import ab.vision.ABType;
import ab.vision.GameStateExtractor.GameState;
import ab.vision.Vision;
import external.ClientMessageEncoder;
import external.ClientMessageTable;
import org.apache.commons.codec.binary.Base64;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 * A server/client version of the java util class that encodes client messages
 * and decodes the corresponding server messages complying with the protocols.
 * Its subclass is ClientActionRobotJava.java which decodes the received server
 * messages into java objects.
 */
public class ClientActionRobot {
	Socket requestSocket;
	OutputStream out;
	InputStream in;
	String message;
	String agentName;

	public ClientActionRobot(String agentName, String... ip) {
		this.agentName = agentName;
		String _ip = "localhost";
		if (ip.length > 0) {
			_ip = ip[0];
		}
		try {
			// 1. creating a socket to connect to the server
			requestSocket = new Socket(_ip, 2004);
			requestSocket.setReceiveBufferSize(100000);
			//System.out.println("Connected to " + _ip + " in port 2004");
			out = requestSocket.getOutputStream();
			out.flush();
			in = requestSocket.getInputStream();
		} catch (UnknownHostException unknownHost) {
			System.err.println("You are trying to connect to an unknown host!");
		} catch (IOException ioException) {
			ioException.printStackTrace();
		}
	}

	public int getNumberOfLevels(){
		int numberLevels;
		try{
			byte[] getGroundTruth = ClientMessageEncoder.encodeNumberOfLevels();
			out.write(getGroundTruth);
			out.flush();
			//System.out.println("client executes command: get number of levels");

			//get return

			byte[] numberLevelsByte = new byte[4];
			in.read(numberLevelsByte);
			numberLevels = bytesToInt(numberLevelsByte);
			//System.out.println("total " +numberLevels + " levels.");
			return numberLevels;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return -1;

	}

	public JSONArray getGroundTruth(){
		JSONArray gt = null;
		try {
			byte[] getGroundTruth = ClientMessageEncoder.encodeGroundTruth();
			out.write(getGroundTruth);
			out.flush();
			//System.out.println("client executes command: get Ground Truth");

			// read how long the ground truth will be
			byte[] gtByteLength = new byte[4];

			in.read(gtByteLength);
			int gtlength = bytesToInt(gtByteLength);
			//System.out.println("Incoming Ground Truth byte length: " + gtlength);

			// record the byte of the ground truth
			byte[] gtbyte = new byte[gtlength];
			byte[] bytebuffer;
			int hasReadBytes = 0;
			while (hasReadBytes < gtlength) {
				bytebuffer = new byte[2048];
				int nBytes = in.read(bytebuffer);
				if (nBytes != -1)
					System.arraycopy(bytebuffer, 0, gtbyte, hasReadBytes, nBytes);
				else
					break;
				hasReadBytes += nBytes;
			}
			String s = new String(gtbyte);

			if (s.length() != 0){
				String sOut = new String(gtbyte).substring(0,gtlength-5); // last 5 char are "|END|"
				////System.out.println(s);
				// transfer it to json
				JSONParser parser = new JSONParser();

				gt = (JSONArray) parser.parse(sOut);

			}

		}
	 	catch (IOException | ParseException ioException) {
		ioException.printStackTrace();
		}
		return gt;
	}

	public JSONArray getNoisyGroundTruth(){
		//System.out.println(agentName + " sent getNoisyGroundTruth");
		JSONArray ngt = null;
		try {
			byte[] getNoisyGroundTruth = ClientMessageEncoder.encodeNoisyGroundTruth();
			out.write(getNoisyGroundTruth);
			out.flush();
			////System.out.println("client executes command: get Noisy Ground Truth");

			// read how long the ground truth will be
			byte[] ngtByteLength = new byte[4];

			in.read(ngtByteLength);
			int ngtlength = bytesToInt(ngtByteLength);
			//System.out.println(agentName + " Incoming Noisy Ground Truth byte length: " + ngtlength);

			// record the byte of the ground truth
			byte[] gtbyte = new byte[ngtlength];
			byte[] bytebuffer;
			int hasReadBytes = 0;
			while (hasReadBytes < ngtlength) {
				bytebuffer = new byte[2048];
				int nBytes = in.read(bytebuffer);
				if (nBytes != -1)
					System.arraycopy(bytebuffer, 0, gtbyte, hasReadBytes, nBytes);
				else
					break;
				hasReadBytes += nBytes;
			}
			String s = new String(gtbyte);
			//System.out.println(agentName + " get 1 getNoisyGroundTruth");

			if (s.length() != 0){
				String sOut = new String(gtbyte).substring(0,ngtlength-5); // last 5 char are "|END|"
				////System.out.println(s);
				// transfer it to json
				JSONParser parser = new JSONParser();

				ngt = (JSONArray) parser.parse(sOut);

			}
			//System.out.println(agentName + " get getNoisyGroundTruth");

			return ngt;


		}
		catch (IOException | ParseException ioException) {
			ioException.printStackTrace();
		}
		return ngt;
	}

	public BufferedImage doScreenShot() {
		BufferedImage bfImage = null;

		try {
			// 2. get Input and Output streams
			byte[] doScreenShot = ClientMessageEncoder.encodeDoScreenShot();
			out.write(doScreenShot);
			out.flush();
			////System.out.println("client executes command: screen shot");

			// Read the message head : 4-byte width and 4-byte height,
			// respectively
			byte[] bytewidth = new byte[4];
			byte[] byteheight = new byte[4];
			int width, height;
			in.read(bytewidth);
			width = bytesToInt(bytewidth);
			in.read(byteheight);
			height = bytesToInt(byteheight);

			// initialize total bytes of the screenshot message
			// not include the head
			// width = 840;
			// height = 480;
			int totalBytes = width * height * 3;

			// read the raw RGB data
			byte[] bytebuffer;
			////System.out.println("width: " + width + "  height: " + height);
			byte[] imgbyte = new byte[totalBytes];
			int hasReadBytes = 0;
			while (hasReadBytes < totalBytes) {
				bytebuffer = new byte[2048];
				int nBytes = in.read(bytebuffer);
				////System.out.println("nBytes " + nBytes);
				if (nBytes != -1)
					System.arraycopy(bytebuffer, 0, imgbyte, hasReadBytes, nBytes);
				else
					break;
				hasReadBytes += nBytes;
			}

			// set RGB data using BufferedImage
			bfImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
			for (int y = 0; y < height; y++) {
				for (int x = 0; x < width; x++) {
					int R = imgbyte[(y * width + x) * 3] & 0xff;
					int G = imgbyte[(y * width + x) * 3 + 1] & 0xff;
					int B = imgbyte[(y * width + x) * 3 + 2] & 0xff;
					Color color = new Color(R, G, B);
					int rgb;
					rgb = color.getRGB();
					bfImage.setRGB(x, y, rgb);
				}
			}

		} catch (IOException ioException) {
			ioException.printStackTrace();
		}

		return bfImage;

	}

	// convert a byte[4] array to int value
	public static int bytesToInt(byte... b) {
		int value = 0;
		for (int i = 0; i < 4; i++) {
			int shift = (4 - 1 - i) * 8;
			value += (b[i] & 0x000000FF) << shift;
		}
		return value;
	}

	// convert an int value to byte[4] array
	public static byte[] intToByteArray(int a) {
		byte[] ret = new byte[4];
		ret[3] = (byte) (a & 0xFF);
		ret[2] = (byte) ((a >> 8) & 0xFF);
		ret[1] = (byte) ((a >> 16) & 0xFF);
		ret[0] = (byte) ((a >> 24) & 0xFF);
		return ret;
	}

	// send message to fully zoom out
	public byte fullyZoomOut() {
		try {
			out.write(ClientMessageEncoder.fullyZoomOut());
			out.flush();

			return (byte) in.read();
		} catch (IOException e) {

			e.printStackTrace();
		}
		return 0;

	}

	// send message to fully zoom out
	public byte fullyZoomIn() {
		try {
			out.write(ClientMessageEncoder.fullyZoomIn());
			out.flush();
			return (byte) in.read();
		} catch (IOException e) {

			e.printStackTrace();
		}
		return 0;

	}

	public byte clickInCenter() {
		try {
			out.write(ClientMessageEncoder.clickInCenter());
			out.flush();
			return (byte) in.read();
		} catch (IOException e) {

			e.printStackTrace();
		}
		return 0;

	}

	// register team id
	public byte[] configure(byte[] team_id,byte[] mode) {
		try {
			out.write(ClientMessageEncoder.configure(team_id,mode));
			out.flush();
			byte[] result = new byte[4];
			in.read(result);

			return result;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	public byte[] readyForNewSet() {
		try {
			//System.out.println(agentName + " Ready for new data set with appropriate agent.");
			out.write(ClientMessageEncoder.encodeReadyForNewSet());
			out.flush();
			byte[] inbuffer = new byte[19];
			in.read(inbuffer);
			return inbuffer;
		} catch (IOException e) {

			e.printStackTrace();
		}
		return new byte[] { 0 };
	}

	// merge byte arrays into one array
	public static byte[] mergeArray(final byte[]... arrays) {
		int size = 0;
		for (byte[] a : arrays) {
			size += a.length;
		}
		byte[] res = new byte[size];

		int destPos = 0;
		for (int i = 0; i < arrays.length; i++) {
			if (i > 0)
				destPos += arrays[i - 1].length;
			int length = arrays[i].length;
			System.arraycopy(arrays[i], 0, res, destPos, length);
		}
		return res;
	}

	public byte reportNoveltyLikelihood(float report_novelty_likelihood,float non_novelty_likelihood, int[] novelObjectIds, int noveltyLevel,
										String noveltyDescription) {
		try {
			//to bytes
			byte[] byteidLength = intToByteArray(novelObjectIds.length);
			byte[] byteencodedDescription = noveltyDescription.getBytes(StandardCharsets.UTF_8);
			byte[] bytedescriptionLength = intToByteArray(byteencodedDescription.length);
			byte[][] idArray = new byte[novelObjectIds.length][4];

			for (int ObjId = 0; ObjId < novelObjectIds.length; ObjId ++){
				idArray[ObjId] = intToByteArray(novelObjectIds[ObjId]);
			}

			byte[] byteid = mergeArray(idArray);
			byte[] bytenoveltyLevel = intToByteArray(noveltyLevel);


			byte[] byteNoveltyLikelihood = floatToByteArray(report_novelty_likelihood);
			byte[] byteNonNoveltyLikelihood = floatToByteArray(non_novelty_likelihood);


			//System.out.println(agentName + " report novelty likelihood");
			out.write(ClientMessageEncoder.encodeReportNoveltyLikelihood(byteNoveltyLikelihood,byteNonNoveltyLikelihood,
					byteidLength, byteid, bytenoveltyLevel, bytedescriptionLength,  byteencodedDescription));
			out.flush();
			byte result = (byte) in.read();
			//System.out.println(agentName + " report response:" + result);
			return result;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return 0;
	}

//	public byte reportNoveltyDescription() {
//		try {
//
//			out.write(ClientMessageEncoder.encodeReportNoveltyDescription());
//			return (byte) in.read();
//		} catch (IOException e) {
//
//			e.printStackTrace();
//		}
//		return 0;
//	}


	// load a certain level
	public byte[] loadNextAvailableLevel() {
		try {
			byte[] inbuffer = new byte[4];
			out.write(ClientMessageEncoder.encodeLoadNextAvailableLevel());
			out.flush();
			//System.out.println(agentName + " sent loadNextAvailableLevel");
			in.read(inbuffer);
			//System.out.println(agentName + " load next level return: " + bytesToInt(inbuffer) );

			if ( bytesToInt(inbuffer) < 0 ||  bytesToInt(inbuffer) > 500) {
				System.exit(1);
			}


			return inbuffer;
		} catch (IOException e) {

			e.printStackTrace();
		}
		return new byte[] { 0 };
	}

	// load a certain level
	public byte[] getNoveltyInfo() {
		try {
			////System.out.println("get novelty info");
			byte[] inbuffer = new byte[4];
			out.write(ClientMessageEncoder.encodeNoveltyInfo());
			out.flush();
			//System.out.println(agentName + " get novelty info...out..");
			in.read(inbuffer);
			//System.out.println(agentName + " get novelty info...done..");
			return inbuffer;
		} catch (IOException e) {

			e.printStackTrace();
		}
		return new byte[] { 0 };	}

	public byte setSimulationSpeed(byte[] i) {
		try {
			//System.out.println(agentName + " sent setSimulationSpeed");

			out.write(ClientMessageEncoder.setSimulationSpeed(i));
			out.flush();
			//System.out.println("get setSimulationSpeed");
			return (byte) in.read();
		} catch (IOException e) {

			e.printStackTrace();
		}
		return 0;
	}

	// send a message to restart the level
	public byte restartLevel() {
		try {
			out.write(ClientMessageEncoder.restart());
			out.flush();
			return (byte) in.read();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return 0;

	}

	// send a shot message to execute a shot in the safe mode
	public byte[] shoot(byte[] fx, byte[] fy, byte[] dx, byte[] dy, byte[] t1, byte[] t2, boolean polar) {
		byte[] inbuffer = new byte[16];
		try {
			if (polar)
				out.write(ClientMessageEncoder.pshoot(dx, dy, t1, t2));
			else
				out.write(ClientMessageEncoder.cshoot(dx, dy, t1, t2));
			out.flush();
			in.read(inbuffer);

			return inbuffer;
		} catch (IOException e) {
			e.printStackTrace();
		}

		return new byte[] { 0 };
	}

	// send a shot message to execute a shot in the fast mode
	public byte[] shootFast(byte[] fx, byte[] fy, byte[] dx, byte[] dy, byte[] t1, byte[] t2, boolean polar,
			TrajectoryPlanner tp, Rectangle sling, ABType birdOnSling, List<ABObject> blocks,
			List<ABObject> birds, int nOfShots) {
		byte[] inbuffer = new byte[16];
		try {
			if (polar)
				out.write(ClientMessageEncoder.pFastshoot(fx, fy, dx, dy, t1, t2));
			else
				out.write(ClientMessageEncoder.cFastshoot(fx, fy, dx, dy, t1, t2));
			out.flush();

			in.read(inbuffer);

			// trajectory check
			//waitingForSceneToBeSteady(birds, birdOnSling);

			return inbuffer;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return new byte[] { 0 };
	}

	public byte shootSafe(byte[] fx, byte[] fy, byte[] dx, byte[] dy, byte[] t1, byte[] t2, boolean polar,
			TrajectoryPlanner tp, Rectangle sling, ABType birdOnSling, List<ABObject> blocks,
			List<ABObject> birds, int nOfShots) {

		if (fx.length != 4 || fy.length != 4 || dx.length != 4 || dy.length != 4 ||t1.length != 4 ||t2.length != 4){

			System.exit(1);
		}

		try {
			if (polar)
				// my change
				// out.write(ClientMessageEncoder.pFastshoot(fx, fy, dx, dy, t1,
				// t2));
				out.write(ClientMessageEncoder.pshoot(dx, dy, t1, t2));
			else
				// my change
				// out.write(ClientMessageEncoder.cFastshoot(fx, fy, dx, dy, t1,
				// t2));
				out.write(ClientMessageEncoder.cshoot(dx, dy, t1, t2));
			out.flush();
			//System.out.println(agentName + " sent shootSafe");

			// trajectory check
			//waitingForSceneToBeSteady(birds, birdOnSling);

			byte ret = (byte) in.read();

			//System.out.println(agentName + " get shootSafe:" + ret);
			return ret;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return 0;
	}

	// send a sequence of shots message
	public byte[] cshootSequence(byte[]... shots) {
		byte[] inbuffer = new byte[16];

		byte[] msg = ClientMessageEncoder.mergeArray(
				new byte[] { ClientMessageTable.getValue(ClientMessageTable.shootSeq) },
				new byte[] { (byte) shots.length });
		for (byte[] shot : shots) {
			msg = ClientMessageEncoder.mergeArray(msg,
					new byte[] { ClientMessageTable.getValue(ClientMessageTable.cshoot) }, shot);
		}

		try {
			out.write(msg);
			out.flush();

			in.read(inbuffer);
			return inbuffer;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return new byte[] { 0 };
	}

	// send a message to get the current state
	public byte getState() {
		try {
			//System.out.println(agentName + " sent get state");
			out.write(ClientMessageEncoder.getState());
			out.flush();
			int state = in.read();
			//System.out.println(agentName + " state: " + state);

			return (byte) state;
		} catch (IOException e) {
			e.printStackTrace();
		}

		return 0;
	}

	// send a message to score of each level
	public byte[] getBestScores(int level) {
		//int level = 21;
		//System.out.println("sent getBestScores");

		int totalBytes = level * 4;
		byte[] buffer = new byte[totalBytes];
		try {
			out.write(ClientMessageEncoder.getBestScores());
			out.flush();

			in.read(buffer);
			return buffer;

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return buffer;
	}

	// send a message to score of each level
	public byte[] getMyScore(int level) {
		//int level = 50;
		//System.out.println("sent getMyScore");

		int totalBytes = level * 4;
		byte[] buffer = new byte[totalBytes];
		try {
			out.write(ClientMessageEncoder.getMyScore());
			out.flush();
			in.read(new byte[4]); // the first 4 bytes are not useful for this agent
			in.read(buffer);
			return buffer;

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return buffer;
	}

	/* Checks and waits until no objects are moving in the screenshot */
	public void waitingForSceneToBeSteady(List<ABObject> birdsOld, ABType birdOnSling) {
		int last = -1;
		long tStart = System.currentTimeMillis() - 2;

		while (GameState.values()[getState()] == GameState.PLAYING && System.currentTimeMillis() - tStart < 22000) {
			////System.out.println("in game state playing, in while loop");
			BufferedImage screen = doScreenShot();
			////System.out.println("after doScreenShot()");
			// erase birds
			Graphics2D g2d = screen.createGraphics();
			////System.out.println("after screen.createGraphics()");

			for (ABObject bird : birdsOld) {
				g2d.setColor(new Color(148, 206, 222));
				g2d.fillRect(bird.getCenter().x - 40, bird.getCenter().y - 40, 80, 80);

			}

			Vision vision = new Vision(screen);
			int total = 0;

			List<ABObject> possibleBirdsFromSling = new ArrayList<ABObject>();

			List<ABObject> birds = vision.findBirdsMBR();

			for (int i = 0; i < birds.size(); ++i) {
				ABObject bird = birds.get(i);

				if (bird.type == birdOnSling && bird.type != ABType.BlueBird) {
					possibleBirdsFromSling.add(bird);
				}

				total += bird.getTotal();
			}

			List<ABObject> pigs = vision.findPigsRealShape();
			for (ABObject pig : pigs) {
				total += pig.getTotal();
			}

			List<ABObject> blocks = vision.findBlocksRealShape();
			for (ABObject block : blocks) {
				total += block.getTotal();
			}

			if (Math.abs(last - total) < 2 && possibleBirdsFromSling.size() == 0 && pigs.size() != 0) {
				break;
			}

			last = total;

			try {
				Thread.sleep(700);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}



	}
	public static byte[] floatToByteArray(float value) {
		int intBits =  Float.floatToIntBits(value);
		return new byte[] {
				(byte) (intBits >> 24), (byte) (intBits >> 16), (byte) (intBits >> 8), (byte) (intBits) };
	}

	public int[] decodeNewSet(byte[] result){
		int timeLimit;
		int interactionLimit;
		int numberOfLevels;
		int attemptsPerLevel;
		int mode;
		int ifAsSet;
		int ifAllowQuery;
		timeLimit = bytesToInt(result[0],result[1],result[2],result[3]);
		interactionLimit = bytesToInt(result[4],result[5],result[6],result[7]);
		numberOfLevels = bytesToInt(result[8],result[9],result[10],result[11]);
		attemptsPerLevel = bytesToInt(result[12],result[13],result[14],result[15]);
		mode = result[16];
		ifAsSet = result[17];
		ifAllowQuery = result[18];

		return new int[]{timeLimit,interactionLimit,numberOfLevels,attemptsPerLevel,mode,ifAsSet,ifAllowQuery};
	}


	public static void main(String args[]) {
		ClientActionRobot robot = new ClientActionRobot("agent1");
		byte[] id = { 1, 1, 2, 3, 4, 0 };
		robot.configure(id,new byte[] {0});
		while (true)
			robot.doScreenShot();
	}


}
