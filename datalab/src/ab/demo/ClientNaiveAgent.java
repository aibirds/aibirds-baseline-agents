/*****************************************************************************
 ** ANGRYBIRDS AI AGENT FRAMEWORK
 ** Copyright (c) 2015,  XiaoYu (Gary) Ge, Stephen Gould,Jochen Renz
 ** Sahan Abeyasinghe, Jim Keys,   Andrew Wang, Peng Zhang
 ** Team DataLab Birds: Karel Rymes, Radim Spetlik, Tomas Borovicka
 ** All rights reserved.
 **This work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License.
 **To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-sa/3.0/
 *or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.
 *****************************************************************************/
package ab.demo;

import java.awt.Point;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.awt.geom.Area;
import java.awt.Graphics2D;
import java.awt.Color;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.LinkedList;
import java.util.Map;
import java.util.Random;
import java.util.Comparator;
import java.util.Collections;

import java.io.*;
import java.util.concurrent.TimeUnit;

import ab.GroundTruth;
import ab.demo.other.Shot;
import ab.demo.other.ClientActionRobot;
import ab.demo.other.ClientActionRobotJava;

import ab.planner.TrajectoryPlanner;

import ab.utils.StateUtil;
import ab.utils.ABUtil;

import ab.vision.ABObject;
import ab.vision.ABType;
import ab.vision.GameStateExtractor.GameState;
import ab.vision.Vision;
import ab.vision.real.shape.*;

import dl.heuristics.*;
import dl.utils.*;
import org.json.simple.JSONArray;

public class ClientNaiveAgent implements Runnable 
{
	//Wrapper of the communicating messages
	private ClientActionRobotJava actionRobot;
	private Random randomGenerator;
	private int trainingLevelBacckup = 1;
	private int currentLevelInt = 1;
	private byte[] currentLevel;
	private boolean changeFromTraining;
	public boolean running = true;
	private int gtPatient = 10;
	private int invalidCount = 0;
	private int repeatedCount = 0;
	private GroundTruth prevGT;
	private int id = 23349;
	private int simulationTime = 50;
	
	private int heuristicId = 0;
	TrajectoryPlanner tp; 	

	//private LogWriter logGamePlay = new LogWriter("outputLevels.csv");
	private boolean firstShot;
	private Point prevTarget;
	private String name;

	/**
	 * Constructor using the default IP
	 * */
	public ClientNaiveAgent(String agentName)
	{
		// the default ip is the localhost
		actionRobot = new ClientActionRobotJava(agentName);
		tp = new TrajectoryPlanner();
		randomGenerator = new Random();
		prevTarget = null;
		firstShot = true;
		name = agentName;
	}
//	public ClientNaiveAgent(int level)
//	{
//		// the default ip is the localhost
//		actionRobot = new ClientActionRobotJava("127.0.0.1");
//		tp = new TrajectoryPlanner();
//		randomGenerator = new Random();
//		prevTarget = null;
//		levelSchemer.currentLevel = (byte)(level );
//		firstShot = true;
//	}
	/**
	 * Constructor with a specified IP
	 * */
//	public ClientNaiveAgent(String ip)
//	{
//		actionRobot = new ClientActionRobotJava(ip);
//		tp = new TrajectoryPlanner();
//		randomGenerator = new Random();
//		prevTarget = null;
//		firstShot = true;
//
//	}
	public ClientNaiveAgent(String ip, int id)
	{
		actionRobot = new ClientActionRobotJava(ip);
		tp = new TrajectoryPlanner();
		randomGenerator = new Random();
		prevTarget = null;
		firstShot = true;
		this.id = id;
	}

	public void run() 
	{
		try {
			Thread.sleep(6000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		byte[] info = actionRobot.configure(ClientActionRobot.intToByteArray(id),new byte[] {1});

		actionRobot.setSimulationSpeed(ClientActionRobot.intToByteArray(simulationTime));
		GameState state;
		while (running)
		{
			state = solve();
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
//			try{
//				state = solve();
//			}
//			catch (Exception e) {
//				repeatedCount = 0;
//				state = GameState.LOST;
//				System.err.println("Error in sovling the level, state set to lost : " + e);
//			}


			//If the level is solved , go to the next level
			if (state == GameState.WON) 
			{
				repeatedCount = 0;
//				int numberOfLevel = actionRobot.getNumberOfLevels();
//				if (currentLevelInt < numberOfLevel) {
//					currentLevelInt++;
//				}
//				else
//					currentLevelInt = 1;
//
//				//System.out.println("win, enter next level: " + currentLevelInt);
//				currentLevel = ClientActionRobot.intToByteArray(currentLevelInt);
				actionRobot.loadNextAvailableLevel();
				actionRobot.getNoveltyInfo();

				// make a new trajectory planner whenever a new level is entered
				tp = new TrajectoryPlanner();

				// first shot on this level, try high shot first
				firstShot = true;
				
			}
			//If lost, then restart the level
			else if (state == GameState.LOST) 
			{
				repeatedCount = 0;
//				int numberOfLevel = actionRobot.getNumberOfLevels();
//				if (currentLevelInt < numberOfLevel) {
//					currentLevelInt++;
//				}
//				else
//					currentLevelInt = 1;

//				currentLevel = ClientActionRobot.intToByteArray(currentLevelInt);
				actionRobot.loadNextAvailableLevel();
				actionRobot.getNoveltyInfo();

			}
			else if (state == GameState.LEVEL_SELECTION) {
				repeatedCount = 0;
				////System.out.println("unexpected level selection page, go to the last current level : "
				//		+ currentLevel);
				actionRobot.loadNextAvailableLevel();
				actionRobot.getNoveltyInfo();

			}
			else if (state == GameState.MAIN_MENU) {
				repeatedCount = 0;
				////System.out.println("unexpected main menu page, reload the level : " + currentLevel);
				//actionRobot.loadLevel(ClientActionRobot.intToByteArray(currentLevelInt));
				actionRobot.loadNextAvailableLevel();
				actionRobot.getNoveltyInfo();

			}
			else if (state == GameState.EPISODE_MENU) {
				////System.out.println(
				//		"unexpected episode menu page, reload the level: " + currentLevel);
				//actionRobot.loadLevel(ClientActionRobot.intToByteArray(currentLevelInt));
				actionRobot.loadNextAvailableLevel();
				actionRobot.getNoveltyInfo();

			}
			else if (state == GameState.REQUESTNOVELTYLIKELIHOOD) {
				float noveltyLikelihood = 0.1f;
				float nonNoveltyLikelihood = 0.9f;
				int[] novelObjectIds = {-1,3,4};
				int noveltyLevel = 0;
				String noveltyDescription = "";

				actionRobot.reportNoveltyLikelihood(noveltyLikelihood,nonNoveltyLikelihood, novelObjectIds, noveltyLevel,noveltyDescription );
				////System.out.println("REQUESTNOVELTYLIKELIHOOD");
			}
			else if (state == GameState.NEWTESTSET){
				//System.out.println("NEWTESTSET state received");
				repeatedCount = 0;

				int[] newConfig = actionRobot.decodeNewSet(actionRobot.readyForNewSet());
				if (changeFromTraining){
					trainingLevelBacckup = currentLevelInt;
				}
				currentLevelInt = 1;
				currentLevel = ClientActionRobot.intToByteArray(currentLevelInt);;
				changeFromTraining = false;
			}
			else if (state == GameState.NEWTRIAL){
				//System.out.println("NEWTRIAL state received");
				repeatedCount = 0;
				int[] newConfig = actionRobot.decodeNewSet(actionRobot.readyForNewSet());
				trainingLevelBacckup = 1;
				currentLevelInt = 1;
				currentLevel = ClientActionRobot.intToByteArray(currentLevelInt);;
			}
			else if (state == GameState.NEWTRAININGSET){
				//System.out.println("NEWTRAININGSET state received");
				repeatedCount = 0;
				int[] newConfig = actionRobot.decodeNewSet(actionRobot.readyForNewSet());
				currentLevelInt = 1;
				currentLevel = ClientActionRobot.intToByteArray(currentLevelInt);;
				trainingLevelBacckup = 1;
				changeFromTraining = true;
			}
			else if (state == GameState.RESUMETRAINING){
				//System.out.println("RESUMETRAINING state received");
				repeatedCount = 0;

				int[] newConfig = actionRobot.decodeNewSet(actionRobot.readyForNewSet());
				currentLevelInt = trainingLevelBacckup;
				currentLevel = ClientActionRobot.intToByteArray(currentLevelInt);;
				changeFromTraining = true;
			}
			else if (state == GameState.EVALUATION_TERMINATED){
				//System.out.println("Evaluation terminated.");
				running = false;
			}
		}

	}

	  /** 
	   * Solve a particular level by shooting birds directly to pigs
	   * @return GameState: the game state after shots.
	 */
	public GameState solve() {

		GameState startState = actionRobot.checkState();

		if (startState != GameState.PLAYING)
		{
			return startState;
		}
		JSONArray gtjson = actionRobot.getNoisyGroundTruth();
		GroundTruth gt = new GroundTruth(gtjson);
		//System.out.println(name + " first ngt");
		////System.out.println(gtjson);

		Rectangle sling = gt.findSlingshotRealShape();

		ABType birdOnSling = gt.getBirdTypeOnSling();

		final List<ABObject> pigs = gt.findPigsRealShape();
		final List<ABObject> birds = gt.findBirdsRealShape();
		final List<ABObject> hills = gt.findHills();
		final List<ABObject> blocks = gt.findBlocksRealShape();

		int gnd = gt.getGroundLevel();
		tp.ground = gnd;

//		// Get game state.
		GameState state = startState;
		if (startState == GameState.PLAYING)
		{
			repeatedCount++;
			////System.out.println("repeatedCount:" + repeatedCount);

			if (repeatedCount >= gtPatient){
				invalidCount = 0;
				repeatedCount = 0;
				//System.out.println("repeated count greater than " + gtPatient + " , game state set to lost.");
				return GameState.LOST;
			}
		}
		if (startState != GameState.PLAYING){
			repeatedCount = 0;
			return startState;
		}

		if (birdOnSling.id == 0){
			repeatedCount += 1;
			//System.out.println("bird on sling is not identified");
			return startState;
		}

		final LogWriter log = new LogWriter("output.csv");
		//accumulates information about the scene that we are currently playing
		SceneState currentState = new SceneState(pigs, hills, blocks, sling, gt.findTNTs(), prevTarget, firstShot,
				birds, birdOnSling);
		Shot shot = null;
			
		// if there is a sling, then play, otherwise just skip.
		if (sling != null)
		{
			
			if (!pigs.isEmpty())
			{
				//shot = findHeuristicAndShoot(currentState, log);
				try{
					shot = findHeuristicAndShoot(currentState, log);
					//System.out.println(name + " found herustic shoot");
				}

				catch(Exception e) {
					//  if there's an error in find heruistic and shot function, shot randomly
					//System.err.println("Error in finding shoots, will try to shoot randomly: " + e);
					System.exit(1);
					shot = DLUtils.findRandomShot(tp, currentState._sling,currentState._birdOnSling);

				}
			}
			else
			{
				//System.err.println("didn't find any pigs.. try shoot randomly");

				//System.err.println("No Release Point Found, will try to shoot randomly...");
				shot = DLUtils.findRandomShot(tp, currentState._sling,currentState._birdOnSling);


			}
			// check whether the slingshot is changed. the change of the slingshot indicates a change in the scale.
			state = performTheActualShooting(log, currentState, shot);

		}

		return state;
	}

	private Shot findHeuristicAndShoot(SceneState currentState, LogWriter log)
	{
		Random rand = new Random();  
		AbstractHeuristic possibleHeuristics [] = new AbstractHeuristic[4];
		possibleHeuristics[0] = new BuildingHeuristic(currentState,actionRobot, tp, log);
		possibleHeuristics[1] = new DestroyAsManyPigsAtOnceAsPossibleHeuristic(currentState,actionRobot, tp, log);
		possibleHeuristics[2] = new RoundStoneHeuristic(currentState,actionRobot, tp, log);
		possibleHeuristics[3] = new DynamiteHeuristic(currentState,actionRobot, tp, log);

		int heuristicId = 0;
		int max = 0xffff0000;

		for (int i = 0; i < possibleHeuristics.length; ++ i)
		{
			AbstractHeuristic tmp = possibleHeuristics[i];

			////System.out.println("Utility " + tmp.toString() + ": " + tmp.getUtility());
			if (max < tmp.getUtility() && 
				!(i == 0 && tmp.getUtility() < 1000) )
			{
				max = tmp.getUtility();
				heuristicId = i;
			}
			
		}
		
		Shot shot = possibleHeuristics[heuristicId].getShot();
		if (shot != null)
		{
			////System.out.println("Heuristic ID: " + heuristicId);
		}

		// we don't include traj for this time
		// if there are hills in the way, choose a random set of blocks and make them targets
//		if (possibleHeuristics[heuristicId].getSelectedDLTrajectory() != null
//			&& possibleHeuristics[heuristicId].getSelectedDLTrajectory().hillsInTheWay.size() != 0)
//		{
//
//			Shot lastBreathShot = getLastBreathShot(currentState, log);
//
//			if (lastBreathShot != null)
//			{
//				shot = lastBreathShot;
//			}
//		}

		if( shot == null )
		{
			shot = DLUtils.findRandomShot(tp, currentState._sling,currentState._birdOnSling);
		}

		return shot;
	}

	private Shot getLastBreathShot(SceneState currentState, LogWriter log)
	{
		List<ABObject> randomBlocks = new ArrayList<ABObject>();

		int rndNumber = 0;
		int nOfRandomBlockToChoose = (currentState._blocks.size() > 5 ? 5 : currentState._blocks.size());					

		for (int i = 0; i < nOfRandomBlockToChoose; i++)
		{
			rndNumber = randomGenerator.nextInt(currentState._blocks.size());

			if (!randomBlocks.contains(currentState._blocks.get(rndNumber)))
			{
				randomBlocks.add(currentState._blocks.get(rndNumber));
			}
			else
			{
				i--;
			}
		}

		AbstractHeuristic lastBreathHeuristic = new DestroyAsManyPigsAtOnceAsPossibleHeuristic(currentState,actionRobot, tp, log);

		return lastBreathHeuristic.getShot();

	}

	private GameState performTheActualShooting(LogWriter log, SceneState currentState, Shot shot) {
		actionRobot.fullyZoomOut();
		JSONArray gtjson = actionRobot.getNoisyGroundTruth();
		//System.out.println(name + " shoot ngt");
		GroundTruth gt = new GroundTruth(gtjson);
		Rectangle _sling = gt.findSlingshotRealShape();

		GameState state = null;
		if (_sling != null) {
			double scale_diff = Math.pow((currentState._sling.width - _sling.width), 2)
					+ Math.pow((currentState._sling.height - _sling.height), 2);

			if (scale_diff < 25) {
				if (shot.getDx() < 0) {
					actionRobot.shoot(shot.getX(), shot.getY(), shot.getDx(), shot.getDy(), 0, shot.getT_tap(), false,
							tp, currentState._sling, currentState._birdOnSling, currentState._blocks,
							currentState._birds, 1);
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}

				try {
					state = actionRobot.checkState();
					//log.appendScore(0, state);
					//log.flush(actionRobot.doScreenShot());
				} catch (Exception e) {
					e.printStackTrace();
				}

				if (state == GameState.PLAYING) {
					//vision = new Vision(actionRobot.doScreenShot());
					//List<Point> traj = gt.findTrajPoints();
					Point releasePoint = new Point(shot.getX() + shot.getDx(), shot.getY() + shot.getDy());

					// adjusts trajectory planner
					Rectangle sling = gt.findSlingshotRealShape();
//                    if (sling != null) {
//                        tp.adjustTrajectory(traj, gt.findSlingshotRealShape(), releasePoint);
//                    }

					firstShot = false;
				}
			} else{
				////System.out.println("Scale is changed, can not execute the shot, will re-segement the image");

			}
		} else{
			////System.out.println("no sling detected, can not execute the shot, will re-segement the image");
		}

		return state;
	}


	public static void main(String args[]) 
	{

		ClientNaiveAgent na;
		if (args.length > 0)
			na = new ClientNaiveAgent(args[0]);
		else
			na = new ClientNaiveAgent("one");
		na.run();
		
	}
}
