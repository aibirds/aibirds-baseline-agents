/*****************************************************************************
 ** ANGRYBIRDS AI AGENT FRAMEWORK
 ** Copyright (c) 2015,  XiaoYu (Gary) Ge, Stephen Gould,Jochen Renz
 ** Sahan Abeyasinghe, Jim Keys,   Andrew Wang, Peng Zhang
 ** Team DataLab Birds: Karel Rymes, Radim Spetlik, Tomas Borovicka
 ** All rights reserved.
 **This work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License.
 **To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-sa/3.0/
 *or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.
 *****************************************************************************/
package ab.demo;

public class MainEntry {
	public static class AgentRunnable implements Runnable {
		String agentName;
		public void run() {
			ClientNaiveAgent na = new ClientNaiveAgent(agentName);
			na.run();
		}
		public AgentRunnable(String agentName){
			this.agentName = agentName;
		}
	}

	// the entry of the software.
	public static void main(String args[]) throws InterruptedException {
		int NUM_OF_AGENT = Integer.parseInt(args[0]);
		for (int i = 0; i < NUM_OF_AGENT; i++) {
			AgentRunnable agent = new AgentRunnable(String.valueOf(i));
			Thread agentThread = new Thread(agent);
			//System.out.println("agent new thread");
			agentThread.start();
			Thread.sleep(10000);
			//System.out.println("agent started");
		}
		//System.exit(0);
	}
}
//		String command = "";
//		if (args.length > 0)
//		{
//			command = args[0];
//			if (args.length == 1)
//			{
//				//int level = Integer.parseInt(args[0]);
//				ClientNaiveAgent na = new ClientNaiveAgent(args[0]);
//				//ClientNaiveAgent na = new ClientNaiveAgent(level);
//				na.run();
//			}



//			else if (args.length == 2)
//			{
//				int id = Integer.parseInt(args[1]);
//				ClientNaiveAgent na = new ClientNaiveAgent(args[0],id);
//
//				na.run();
//			}
//
//		}
//		else
//			System.out.println("Please input the correct command");
//
//		System.exit(0);
//	}
//}