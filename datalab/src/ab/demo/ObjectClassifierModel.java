package ab.demo;

import ab.vision.ABType;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.util.Scanner;
import java.io.File;
import java.net.*;

public class ObjectClassifierModel {
    public static double[][] readArray(String name){
	BufferedReader txtReader = null;	
/*	URL test1 = ObjectClassifierModel.class.getResource("/");
	URL test2 = ObjectClassifierModel.class.getResource(".");            
	URL test3 = ObjectClassifierModel.class.getClassLoader().getResource("../");

	System.out.println(test1); 
	System.out.println(test2.getPath());
	System.out.println(test3.getPath());*/
	try{
	    txtReader = new BufferedReader(new InputStreamReader(ObjectClassifierModel.class.getResourceAsStream("/model")));
	}     
	catch (Exception e) {
            
	    e.printStackTrace();
    	}
        Scanner sc = new Scanner(txtReader);
        int rows = 11;
        int columns = 257;
        double [][] myArray = new double[rows][columns];
        while(sc.hasNextLine()) {
            for (int i=0; i<myArray.length; i++) {
                String[] line = sc.nextLine().trim().split(",");
                for (int j=0; j<line.length; j++) {
                    myArray[i][j] = Double.parseDouble(line[j]);
                }
            }
        }
        return myArray;
    }
    static public double[][] model_coef;
    static  {
        try {
            model_coef = readArray("model");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public static ABType[] target_class = {
            ABType.TNT,
            ABType.BlackBird,
            ABType.BlueBird,
            ABType.RedBird,
            ABType.WhiteBird,
            ABType.YellowBird,
            ABType.Ice,
            ABType.Pig,
            ABType.Hill,
            ABType.Stone,
            ABType.Wood
    };
}
