package ab.demo;

import ab.vision.ABType;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.Arrays;
import java.util.Scanner;

public class ColorTemplate {

    public static double[][] readArray(String name) throws Exception {
        Scanner sc = new Scanner(new BufferedReader(new FileReader(name)));
        int rows = 89;
        int columns = 256;
        double [][] myArray = new double[rows][columns];
        while(sc.hasNextLine()) {
            for (int i=0; i<myArray.length; i++) {
                String[] line = sc.nextLine().trim().split(" ");
                for (int j=0; j<line.length; j++) {
                    myArray[i][j] = Double.parseDouble(line[j]);
                }
            }
        }
        return myArray;
    }

    public static double[][] colorMatrix;

    static {
        try {
            colorMatrix = readArray("ColorTemplate.txt");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static ABType[] colorMatrixType = {
            ABType.RedBird,
            ABType.BlackBird,
            ABType.BlueBird,
            ABType.WhiteBird,
            ABType.YellowBird,
            ABType.BlackBird,
            ABType.BlackBird,
            ABType.BlackBird,
            ABType.BlackBird,
            ABType.BlackBird,
            ABType.BlackBird,
            ABType.BlackBird,
            ABType.BlackBird,
            ABType.BlackBird,
            ABType.BlueBird,
            ABType.BlueBird,
            ABType.BlueBird,
            ABType.BlueBird,
            ABType.BlueBird,
            ABType.BlueBird,
            ABType.RedBird,
            ABType.RedBird,
            ABType.RedBird,
            ABType.RedBird,
            ABType.RedBird,
            ABType.RedBird,
            ABType.WhiteBird,
            ABType.WhiteBird,
            ABType.WhiteBird,
            ABType.WhiteBird,
            ABType.WhiteBird,
            ABType.WhiteBird,
            ABType.WhiteBird,
            ABType.YellowBird,
            ABType.YellowBird,
            ABType.YellowBird,
            ABType.YellowBird,
            ABType.YellowBird,
            ABType.YellowBird,
            ABType.YellowBird,
            ABType.Hill,
            ABType.Ice,
            ABType.Ice,
            ABType.Ice,
            ABType.Ice,
            ABType.Ice,
            ABType.Ice,
            ABType.Ice,
            ABType.Ice,
            ABType.Ice,
            ABType.Ice,
            ABType.Ice,
            ABType.Ice,
            ABType.Pig,
            ABType.Pig,
            ABType.Pig,
            ABType.Pig,
            ABType.Pig,
            ABType.Pig,
            ABType.Pig,
            ABType.Pig,
            ABType.Pig,
            ABType.Pig,
            ABType.Pig,
            ABType.TNT,
            ABType.Stone,
            ABType.Stone,
            ABType.Stone,
            ABType.Stone,
            ABType.Stone,
            ABType.Stone,
            ABType.Stone,
            ABType.Stone,
            ABType.Stone,
            ABType.Stone,
            ABType.Stone,
            ABType.Stone,
            ABType.Wood,
            ABType.Wood,
            ABType.Wood,
            ABType.Wood,
            ABType.Wood,
            ABType.Wood,
            ABType.Wood,
            ABType.Wood,
            ABType.Wood,
            ABType.Wood,
            ABType.Wood,
            ABType.Wood
    };
}
