/*****************************************************************************
 ** ANGRYBIRDS AI AGENT FRAMEWORK
 ** Copyright (c) 2014, XiaoYu (Gary) Ge, Stephen Gould, Jochen Renz
 **  Sahan Abeyasinghe,Jim Keys,  Andrew Wang, Peng Zhang
 ** All rights reserved.
 **This work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License. 
 **To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-sa/3.0/ 
 *or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.
 *****************************************************************************/

package external;

import ab.demo.other.ClientActionRobot;
import ab.demo.other.ClientActionRobotJava;

/*encode the messages to byte[]*/
public class ClientMessageEncoder {


	public static byte[] encodeReadyForNewSet() {
		byte[] message = { ClientMessageTable.getValue(ClientMessageTable.readyforNewSet) };

		return message;

	}

	public static byte[] encodeReportNoveltyLikelihood(byte[] report_novelty_likelihood, byte[] non_novelty_likelihood,
													   byte[] byteidLength, byte[] byteid, byte[] bytenoveltyLevel, byte[] bytedescriptionLength,
													   byte[] byteencodedDescription) {
//		byte[] message = { ClientMessageTable.getValue(ClientMessageTable.reportNoveltyLikelihood),
//				report_novelty_likelihood[0],report_novelty_likelihood[1],report_novelty_likelihood[2],report_novelty_likelihood[3],
//				non_novelty_likelihood[0],non_novelty_likelihood[1],non_novelty_likelihood[2],non_novelty_likelihood[3]};

		byte[] message = mergeArray(new byte[] { ClientMessageTable.getValue(ClientMessageTable.reportNoveltyLikelihood) },
				report_novelty_likelihood, non_novelty_likelihood, byteidLength, byteid, bytenoveltyLevel, bytedescriptionLength, byteencodedDescription);

		return message;
	}

	public static byte[] encodeLoadNextAvailableLevel(){
		byte[] message = { ClientMessageTable.getValue(ClientMessageTable.loadNextAvailableLevel) };
		return message;
	}

	public static byte[] encodeNoveltyInfo(){
		byte[] message = { ClientMessageTable.getValue(ClientMessageTable.noveltyInfo) };
		return message;
	}

//	public static byte[] encodeReportNoveltyDescription() {
//		byte[] message = { ClientMessageTable.getValue(ClientMessageTable.reportNoveltyDescription) };
//		return message;
//
//	}

	// encode get ground truth message
	public static byte[] encodeGroundTruth() {
		byte[] message = { ClientMessageTable.getValue(ClientMessageTable.groundTruth) };
		return message;

	}

	// encode get noisy ground truth message
	public static byte[] encodeNoisyGroundTruth() {
		byte[] message = { ClientMessageTable.getValue(ClientMessageTable.noisyGroundTruth) };
		return message;

	}

	// encode get noisy ground truth message
	public static byte[] encodeNumberOfLevels() {
		byte[] message = { ClientMessageTable.getValue(ClientMessageTable.numberLevels) };
		return message;

	}

	// encode screenshot message
	public static byte[] encodeDoScreenShot() {
		byte[] message = { ClientMessageTable.getValue(ClientMessageTable.doScreenShot) };
		return message;

	}

	// encode configure message
	public static byte[] configure(byte[] id,byte[] mode) {
		byte[] message = new byte[1 + id.length];
		message = mergeArray(new byte[] { ClientMessageTable.getValue(ClientMessageTable.configure) }, id, mode);

		return message;
	}

	// encode loadlevel message
	// allow 0 or 1 input argument
	public static byte[] loadLevel(byte[] level) {
		////System.out.println("encoder: " + level);
		byte[] message = { ClientMessageTable.getValue(ClientMessageTable.loadLevel),
				level[0],level[1],level[2],level[3] };

		return message;
	}

	public static byte[] setSimulationSpeed(byte[] speed){
		byte[] message = {ClientMessageTable.getValue(ClientMessageTable.setSimulationSpeed), speed[0],speed[1],speed[2],speed[3]};
		return message;
	}

	// encode restart message
	public static byte[] restart() {
		byte[] message = { ClientMessageTable.getValue(ClientMessageTable.restartLevel) };
		return message;
	}

	// encode cshoot message (safe mode)
	public static byte[] cshoot(byte[] fx, byte[] fy,  byte[] t1, byte[] t2) {
		byte[] message = new byte[1 + fx.length + fy.length + t1.length + t2.length];
		message = mergeArray(new byte[] { ClientMessageTable.getValue(ClientMessageTable.cshoot) }, fx, fy,  t1,
				t2);
		return message;
	}

	// encode pshoot message (safe mode)
	public static byte[] pshoot(byte[] fx, byte[] fy,  byte[] t1, byte[] t2) {
		byte[] message = new byte[1 + fx.length + fy.length + t1.length + t2.length];
		message = mergeArray(new byte[] { ClientMessageTable.getValue(ClientMessageTable.pshoot) }, fx, fy,  t1,
				t2);
		return message;
	}

	// encode pshoot message in fast mode
	public static byte[] pFastshoot(byte[] fx, byte[] fy, byte[] dx, byte[] dy, byte[] t1, byte[] t2) {
		byte[] message = new byte[1 + fx.length + fy.length + dx.length + dy.length + t1.length + t2.length];
		message = mergeArray(new byte[] { ClientMessageTable.getValue(ClientMessageTable.pFastshoot) }, fx, fy, dx, dy,
				t1, t2);
		return message;
	}

	// encode cshoot message in fast mode
	public static byte[] cFastshoot(byte[] fx, byte[] fy, byte[] dx, byte[] dy, byte[] t1, byte[] t2) {
		byte[] message = new byte[1 + fx.length + fy.length + dx.length + dy.length + t1.length + t2.length];
		message = mergeArray(new byte[] { ClientMessageTable.getValue(ClientMessageTable.cFastshoot) }, fx, fy, dx, dy,
				t1, t2);
		return message;

	}

	// encode fully zoom out message
	public static byte[] fullyZoomOut() {
		byte[] message = { ClientMessageTable.getValue(ClientMessageTable.fullyZoomOut) };
		return message;

	}

	public static byte[] fullyZoomIn() {
		byte[] message = { ClientMessageTable.getValue(ClientMessageTable.fullyZoomIn) };
		return message;

	}

	public static byte[] clickInCenter() {
		byte[] message = { ClientMessageTable.getValue(ClientMessageTable.clickInCentre) };
		return message;
	}

	// encode getState message
	public static byte[] getState() {
		byte[] message = { ClientMessageTable.getValue(ClientMessageTable.getState) };
		return message;
	}

	// encode get best scores message
	public static byte[] getBestScores() {
		byte[] message = { ClientMessageTable.getValue(ClientMessageTable.getBestScores) };
		return message;
	}

	// get my score message
	public static byte[] getMyScore() {
		byte[] message = { ClientMessageTable.getValue(ClientMessageTable.getMyScore) };
		return message;
	}

	// merge byte arrays into one array
	public static byte[] mergeArray(final byte[]... arrays) {
		int size = 0;
		for (byte[] a : arrays) {
			size += a.length;
		}
		byte[] res = new byte[size];

		int destPos = 0;
		for (int i = 0; i < arrays.length; i++) {
			if (i > 0)
				destPos += arrays[i - 1].length;
			int length = arrays[i].length;
			System.arraycopy(arrays[i], 0, res, destPos, length);
		}
		return res;
	}




}
