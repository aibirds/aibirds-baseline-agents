/*****************************************************************************
** ANGRYBIRDS AI AGENT FRAMEWORK
** Copyright (c) 2014, XiaoYu (Gary) Ge, Jochen Renz, Stephen Gould,
**  Sahan Abeyasinghe,Jim Keys,   Andrew Wang, Peng Zhang
** All rights reserved.
**This work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License. 
**To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-sa/3.0/ 
*or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.
*****************************************************************************/

package external;

/**
 * This class maintains all the client messages and its corresponding MIDs.
 */
public enum ClientMessageTable {
	configure(1), doScreenShot(11), loadLevel(51), restartLevel(52), cshoot(31), pshoot(32), cFastshoot(41), pFastshoot(
			42), shootSeqFast(43), clickInCentre(36), getState(12), getMyScore(
					23), fullyZoomOut(34), fullyZoomIn(35), getCurrentLevel(14),
	getBestScores(13), shootSeq(33), numberLevels(15), setSimulationSpeed(2),
	groundTruth(62), noisyGroundTruth(64), currentLevelScore(65),
	reportNoveltyLikelihood(66), reportNoveltyDescription(67), readyforNewSet(68),
	loadNextAvailableLevel(53), noveltyInfo(69);

	@SuppressWarnings("unused")
	private int message_code;

	private ClientMessageTable(int message_code) {
		this.message_code = message_code;
	}

	// map message from int to enum
	public static ClientMessageTable getValue(int message_code) {
		switch (message_code) {
		case 1:
			return configure;
		case 11:
			return doScreenShot;
		case 51:
			return loadLevel;
		case 52:
			return restartLevel;
		case 31:
			return cshoot;
		case 32:
			return pshoot;
		case 12:
			return getState;
		case 34:
			return fullyZoomOut;
		case 35:
			return fullyZoomIn;
		case 14:
			return getCurrentLevel;
		case 13:
			return getBestScores;
		case 23:
			return getMyScore;
		case 33:
			return shootSeq;
		case 41:
			return cFastshoot;
		case 42:
			return pFastshoot;
		case 43:
			return shootSeqFast;
		case 36:
			return clickInCentre;
		case 15:
			return numberLevels;
		case 62:
			return groundTruth;
		case 64:
			return noisyGroundTruth;
		case 66:
			return reportNoveltyLikelihood;
		case 67:
			return reportNoveltyDescription;
		case 68:
			return readyforNewSet;
		case 2:
			return setSimulationSpeed;
		case 53:
			return loadNextAvailableLevel;
		case 69:
			return noveltyInfo;
		}
		return null;
	}

	// map message from enum to byte
	public static byte getValue(ClientMessageTable message) {
		switch (message) {
		case doScreenShot:
			return 11;
		case configure:
			return 1;
		case loadLevel:
			return 51;
		case restartLevel:
			return 52;
		case cshoot:
			return 31;
		case pshoot:
			return 32;
		case getState:
			return 12;
		case fullyZoomOut:
			return 34;
		case getCurrentLevel:
			return 14;
		case getBestScores:
			return 13;
		case shootSeq:
			return 33;
		case cFastshoot:
			return 41;
		case pFastshoot:
			return 42;
		case shootSeqFast:
			return 43;
		case getMyScore:
			return 23;
		case clickInCentre:
			return 36;
		case fullyZoomIn:
			return 35;

		case numberLevels:
			return 15;

		case groundTruth:
			return 62;

		case noisyGroundTruth:
			return 64;

		case reportNoveltyLikelihood:
			return 66;

		case reportNoveltyDescription:
			return 67;

		case readyforNewSet:
			return 68;

		case setSimulationSpeed:
			return 2;

		case loadNextAvailableLevel:
			return 53;

		case noveltyInfo:
			return 69;
		}
		return 0;
	}
}
