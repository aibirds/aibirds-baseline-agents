# The Datalab Agent for Science Birds

## Description
This is the datalab agent that is adapted to the 640x480 resolution. It performs better in the pre-novelty games than the version used for evaluations before M24.

## Requirements
Java version >= 12 
Gradle version >= 4.4.1

## Compile
Under the root folder of the software, run:

  ./gradle build

If the build is successful, a jar file called datalab.jar will be generated in the same folder. 

## Usage
1. start a game_playing_interface
2. java -jar datalab.jar [number of agents]

## Authors and acknowledgment
This work is based on the original Datalab agent by Tomáš Borovička, Radim Špetlík and Karel Rymeš, Czech Technical University.
